--@name EnginesDeviceManager
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

Ship.Engines = {}

local EngineOnMt = "spacebuild/fusion4"
local EngineOffMt = "cmats/base_metal_light"

local ActiveEngs = false

local function StartLargeEngine(ent, b)
    if (b) then
        ent:setMaterial(EngineOnMt)
        ent:setColor(Color(255, 255, 255))
    else
        ent:setMaterial(EngineOffMt)
        ent:setColor(Color(255, 144, 0))
    end
end
local function StartSmallEngine(ent, b, ids)
    if (b) then
        for k, v in pairs(ids) do
            ent:setSubMaterial(v, EngineOnMt)
        end
    else
        for k, v in pairs(ids) do
            ent:setSubMaterial(v, EngineOffMt)
        end
    end
end
local function StartMainEngine(ent, b, data)
    if (b) then
        --The17thDoctor
        local pos = ent:toWorld(data.Center + Vector(data.Size[1] / 2.7, 0, 0))
        local ang = ent:toWorld(Angle(0, 180, 0))
        local size = Vector(0.022 * data.Size[1], 0.063 * data.Size[2], 0.113 * data.Size[3])
        data.Holo = holograms.create(pos, ang, "models/cerus/weapons/projectiles/pc_proj.mdl", size)
        data.Holo:setColor(255, 93, 130)
        data.Holo:setAlpha(254)
        data.Holo:setParent(ent)
    else
        data.Holo:remove()
    end
end

function StartEngines()
    for k, v in pairs(Ship.Engines) do
        v.Func(v.Prop, true, v.Var)
    end
    ActiveEngs = true
    SendAPIEvent("ENGINES_ON")
end
function StopEngines()
    for k, v in pairs(Ship.Engines) do
        v.Func(v.Prop, false, v.Var)
    end
    ActiveEngs = false
    SendAPIEvent("ENGINES_OFF")
end

function EnginesActive()
    return ActiveEngs
end

RegisterDriver({"prop_physics"}, function(ent)
    if (ent:model() == "models/props_phx/construct/metal_dome360.mdl" and ent:material() == "cmats/base_metal_light") then
        table.insert(Ship.Engines, {Prop = ent, Func = StartLargeEngine})
        return true
    elseif (ent:model() == "models/cerus/modbridge/misc/weapons/wep_ion2.mdl") then
        local vars = {
            Holo = nil,
            Center = ent:obbCenter(),
            Size = ent:obbSize()
        }
        table.insert(Ship.Engines, {Prop = ent, Func = StartMainEngine, Var = vars})
        return true
    elseif (ent:model() == "models/cerus/modbridge/misc/engines/eng_p31.mdl"
            or ent:model() == "models/cerus/modbridge/misc/engines/eng_sq11b.mdl"
            or ent:model() == "models/cerus/modbridge/plate/flat/wd11.mdl"
            or ent:model() == "models/cerus/modbridge/plate/flat/wd21.mdl"
            or ent:model() == "models/cerus/modbridge/plate/flat/wd31.mdl"
            or ent:model() == "models/cerus/modbridge/plate/flat/wdc11b.mdl") then
        local ids = {}
        for k, v in pairs(ent:getMaterials()) do
            if (v == "cmats/light" or v == "cmats/base_metal_light") then
                table.insert(ids, k - 1)
            end
        end
        table.insert(Ship.Engines, {Prop = ent, Func = StartSmallEngine, Var = ids})
        return true
    end
    return false
end)

HookPostLoad(StartEngines)
