--@name TimerDeviceManager
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

local IsRunning = false
local TimeRemaining = 0
--NOTE : The time is in seconds
function StartTimerCountdown(time, callback)
	if (IsRunning) then return end
	IsRunning = true
	TimeRemaining = time
	task.Start(function()
		local mins = math.floor(TimeRemaining / 60)
		local secs = TimeRemaining - (mins * 60)
		for k, v in pairs(Ship.Timers) do
			v["Reset"] = 1
			v["Minutes"] = mins
			v["Seconds"] = secs
		end
		TimeRemaining = TimeRemaining - 1
		if (TimeRemaining < 0) then
			TimeRemaining = 0
			IsRunning = false
			for k, v in pairs(Ship.Timers) do
				v["Reset"] = 1
				v["Minutes"] = 0
				v["Seconds"] = 0
			end
			callback()
			task.End("LanAI_TimerCountdown")
			return
		end
	end, "LanAI_TimerCountdown", 1)
end

function EndTimerCountdown()
	task.End("LanAI_TimerCountdown")
	TimeRemaining = 0
	IsRunning = false
	for k, v in pairs(Ship.Timers) do
		v["Reset"] = 1
		v["Minutes"] = 0
		v["Seconds"] = 0
	end
end

function IsTimerInProgress()
	return IsRunning
end

function AddTime(time)
    TimeRemaining = TimeRemaining + time
end

function RemoveTime(time)
    TimeRemaining = TimeRemaining - time
end

Ship.Timers = {}
RegisterDriver({"destiny_timer", "starfall_emitter"}, function(ent)
	if (ent:class() == "starfall_emitter" and starfall.getName(ent) == "DestinyTimer") then
		table.insert(Ship.Timers, ent:getWirelink())
		return true
	elseif (ent:class() == "destiny_timer") then
		ent:getWirelink()["Disable Use"] = 1
		ent:getWirelink()["Normal Font"] = 1
		table.insert(Ship.Timers, ent:getWirelink())
		return true
	end
	return false
end)
