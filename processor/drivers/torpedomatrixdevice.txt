--@name TorpedoMatrixDeviceManager
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

local oldang = nil
local TorpedoMatrix = nil

HookPostLoad(function()
	if (TorpedoMatrix == nil) then
		util.Log("Drivers.TorpedoMatrix", LOG_ERR, "No TorpedoMatrix found on this ship")
		return
	end
	oldang = TorpedoMatrix:ang()
	local old = StartAngles
	function StartAngles()
		old()
		local pos = CalcTargetCenter()
		if (pos == nil) then return end
		oldang = TorpedoMatrix:ang()
		TorpedoMatrix:setAngles((pos - TorpedoMatrix:getPos()):Angle() + Angle(90, 0, 0))
	end
	local old1 = StopWeapons
	function StopWeapons()
		old1()
		TorpedoMatrix:setAngles(oldang)
	end
	task.Start(function()
		if (not(IsFiring())) then return end
		local pos = CalcTargetCenter()
		if (pos == nil) then return end
		TorpedoMatrix:setAngles((pos - TorpedoMatrix:getPos()):Angle() + Angle(90, 0, 0))
	end, "LanAI_TorpedoMatrixUpdater", 1)
end)

RegisterDriver({"prop_physics"}, function(ent)
	if (ent:model() == "models/cerus/modbridge/plate/flat/b111.mdl"
		and ent:material() == "models/XQM/LightLinesRed_tool") then
		TorpedoMatrix = ent
		return true
	end
	return false
end)
