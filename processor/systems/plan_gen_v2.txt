--@name PlanGenV2
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

local Debug = false

local Corners = {
    Vector(-1, -1, -1),
    Vector(-1, -1, 1),
    Vector(-1, 1, -1),
    Vector(-1, 1, 1),
    Vector(1, -1, -1),
    Vector(1, -1, 1),
    Vector(1, 1, -1),
    Vector(1, 1, 1)
}

local function TransferToLocalSpace1(origin, ent)
    local size = ent:obbSize()
    local cmin, cmax
    for j = 1, #Corners do
        local cpos = (Corners[j] * size * 0.5) + ent:obbCenter()
        local posw = ent:localToWorld(cpos)
        local pos = worldToLocal(posw, Angle(0, 0, 0), origin, Angle(0, 0, 0))
        if not cmin or pos.x + pos.y + pos.z < cmin.x + cmin.y + cmin.z then
            cmin = pos
        end
        if not cmax or pos.x + pos.y + pos.z > cmax.x + cmax.y + cmax.z then
            cmax = pos
        end
    end
    cmax.Z = cmin.Z
    return cmin, cmax
end

local function FindBestOrigin(shield, p1, p2, p3, p4)
    local p1w = shield:localToWorld(p1)
    local p2w = shield:localToWorld(p2)
    local p3w = shield:localToWorld(p3)
    local p4w = shield:localToWorld(p4)
    local p1o, _ = TransferToLocalSpace1(p1w, ents.self())
    local p2o, _ = TransferToLocalSpace1(p2w, ents.self())
    local p3o, _ = TransferToLocalSpace1(p3w, ents.self())
    local p4o, _ = TransferToLocalSpace1(p4w, ents.self())
    if (p1o.X >= 0 and p1o.Y >= 0) then
        return p1
    elseif (p2o.X >= 0 and p2o.Y >= 0) then
        return p2
    elseif (p3o.X >= 0 and p3o.Y >= 0) then
        return p3
    else
        return p4
    end
end

util.Log("PlanGen", LOG_INFO, "Initializing plan generator...")
local function CreateShipOrigin()
    local core = Ship.Core:entity()
    local shield = Ship.Shield:entity()
    local min, max = core:getShipSize()
    local p1 = min
    local p2 = Vector(min.X, max.Y, min.Z)
    local p3 = Vector(max.X, min.Y, min.Z)
    local p4 = Vector(max.X, max.Y, min.Z)
    min = FindBestOrigin(shield, p1, p2, p3, p4)
    local origin = holograms.create(shield:localToWorld(min), Angle(0, 0, 0), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
    origin:setParent(ents.self())
    if (Debug) then
        origin:setColor(Color(255, 0, 0))
    else
        origin:setColor(Color(255, 255, 255, 0))
    end
    return origin
end

local Origin = CreateShipOrigin()
local Floors = {}

local function TransferToLocalSpace(ent)
    local size = ent:obbSize()
    local cmin, cmax
    for j = 1, #Corners do
        local cpos = (Corners[j] * size * 0.5) + ent:obbCenter()
        local posw = ent:localToWorld(cpos)
        local pos = Origin:worldToLocal(posw)
        if not cmin or pos.x + pos.y + pos.z < cmin.x + cmin.y + cmin.z then
            cmin = pos
        end
        if not cmax or pos.x + pos.y + pos.z > cmax.x + cmax.y + cmax.z then
            cmax = pos
        end
    end
    return cmin, cmax
end

DefineOSApp("shipplanv2", function()
	CheckVar("Floors", Floors)
	local ent = nil
	if (Target.Entity) then
	    ent = Target.Entity
    elseif (Target.Player) then
        ent = Target.Player
    end
    CheckVar("Target", ent)
    CheckVar("Origin", Origin)
end)

local function FindProperFloor(p1, p4)
    for j = 1, #Floors do
        if (p1.Z >= Floors[j].Min - 5 and p4.Z <= Floors[j].Max + 100) then
            return j
        end
    end
    return 0
end

local function DebugFloors()
    util.Log("PlanGen", LOG_DEBUG, "Generating debug holograms...")
    for j = 1, #Floors do
        util.Log("PlanGen", LOG_DEBUG, "Found floor #", j, " Min=", Floors[j].Min, ", Max=", Floors[j].Max)
        local h = holograms.create(Origin:localToWorld(Vector(0, 0, Floors[j].Min)), Angle(0, 0, 0), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
        h:setColor(Color(255, 255, 0))
    end
end

HookPostLoad(function()
    util.Log("PlanGen", LOG_INFO, "Iterating ship props...")
    SendPacket("PROC", "START", "Generating plans...")
    local nprops = 0
    local nfloors = 0
    bypass.Iterator("ShipPlanGenerator", Ship.EvaProps, function(cur, tbl)
        local p1, p4 = TransferToLocalSpace(tbl.Prop)
        local rect = {}
        rect.X = p1.X * 0.5
        rect.Y = p1.Y * 0.5
        rect.W = (p4.X - p1.X) * 0.5
        rect.H = (p4.Y - p1.Y) * 0.5
        local index = FindProperFloor(p1, p4)
        if (index == 0) then
            local tbl = {}
            tbl.Min = p1.Z
            tbl.Max = p4.Z
            tbl.Rects = {}
            table.insert(tbl.Rects, rect)
            Floors[#Floors + 1] = tbl
            nfloors = nfloors + 1
        else
            table.insert(Floors[index].Rects, rect)
        end
        nprops = nprops + 1
        SendPacket("PROC", "UPD", cur * 100 / #Ship.EvaProps)
    end, function()
        SendPacket("PROC", "UPD", 100)
        SendPacket("PROC", "END")
        util.Log("PlanGen", LOG_INFO, "Generation done, found ", nprops, " rectangle(s) and ", nfloors, " floor(s).")
        table.sort(Floors, function(a, b) return a.Min < b.Min end)
        if (Debug) then
            DebugFloors()
        end
    end)
end)
